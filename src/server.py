from threading import Thread, Semaphore
import socket
import time
from sys import stdout
from time import sleep
import json
import random as rd


def compare_tx(tx):
    return tx["score"]


class Inboxes:
    def __init__(self):
        self.old = []
        self.recent = []

    def update(self):
        global inbox_sema

        t = time.time()
        for tx in self.old:
            if tx["timestamp"] < t - 2 * max_latency:
                self.old.remove(tx)
        for tx in self.recent:
            if tx["timestamp"] < t - max_latency:
                self.old.append(tx)
                self.recent.remove(tx)

        self.old = sorted(self.old, key=compare_tx)
        self.recent = sorted(self.recent, key=compare_tx)
        self.old = self.old[:inbox_size]

    def insert_tx(self, tx):
        global inbox_sema

        inbox_sema.acquire()
        self.update()
        self.recent.append(tx)
        self.recent = sorted(self.recent, key=compare_tx)
        self.recent = self.recent[:inbox_size]
        inbox_sema.release()

    def pop(self):
        global inbox_sema

        inbox_sema.acquire()
        self.update()
        if len(self.old) > 0:
            tx = self.old[0]
            del self.old[0]
            inbox_sema.release()
            return tx
        else:
            inbox_sema.release()
            return None

    def get_size(self):
        global inbox_sema

        inbox_sema.acquire()
        inbox_sema.release()
        return len(self.recent)


class AnalysisThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.inbox_load = ""

    def run(self):
        while continue_analysis == True:
            self.inbox_load += str(int(time.time() - starting_time)) + \
                ";" + str(inboxes.get_size()) + "\n"
            sleep(0.5)

    def flush(self):
        return self.inbox_load


class GeneratingThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global tx_count
        print(tx_count)

        while continue_simulation == True:
            new_tx = {"id": name + "-" + str(tx_count),
                      "timestamp": int(time.time()),
                      "score": rd.randint(1, 10000)}

            snd_ready = (json.dumps(new_tx)).encode("utf-8")

            tx_count += 1

            for node in neighbors:
                clientsocket = socket.socket(
                    socket.AF_INET, socket.SOCK_STREAM)
                try:
                    clientsocket.connect((node["addr"], node["port"]))
                    clientsocket.send(b"T")
                    clientsocket.send(snd_ready)
                    clientsocket.close()
                except ConnectionRefusedError:
                    print("Connection lost, ending the program")
                    exit(1)

            sleep(1 / lamda)

        print("\rG# Connection lost")


class ServerThread(Thread):
    def __init__(self, setup):
        Thread.__init__(self)
        self.setup = setup
        self.setup.begin_master = False

    def run(self):
        global continue_simulation
        setup_done = False

        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.bind((socket.gethostname(), 0))

        addr = serversocket.getsockname()[0]
        port = serversocket.getsockname()[1]

        f = open("ips.txt", "a")
        f.write(str(addr) + ":" + str(port) + "\n")
        f.close()

        print("Server is running")

        while continue_simulation:
            serversocket.listen(5)
            # accept connections from outside
            (clientsocket, address) = serversocket.accept()
            # now do something with the clientsocket
            # in this case, we'll pretend this is a threaded server
            type = clientsocket.recv(1).decode()

            if self.setup.begin_master == False:
                if type == "S":
                    self.setup.socket = clientsocket
                elif type == "B":
                    print("B# Received a Begin signal")
                    self.setup.begin_master = True
            else:
                if type == "T":
                    thread = ReceiveThread(clientsocket)
                    thread.run()
                elif type == "E":
                    print("JE M'ARRETE")
                    output = name + " a fini!\n Il a reçu " + \
                        str(len(received_tx)) + " transactions et a généré " + \
                        str(tx_count) + " transactions."
                    output = analysis.flush()
                    clientsocket.send(output.encode("utf-8"))
                    serversocket.close()
                    continue_simulation = False
                    break


class ReceiveThread(Thread):
    def __init__(self, clientsocket):
        Thread.__init__(self)
        self.socket = clientsocket

    def run(self):
        global tangle
        global old_tx
        global new_tx
        global received_tx
        global inboxes

        new_tx_json = ""
        while 1:
            rec = self.socket.recv(2048)
            if rec == b'':
                break
            else:
                new_tx_json += rec.decode()
        new_tx = json.loads(new_tx_json)
        print(new_tx)

        id = new_tx["id"]

        if id not in received_tx:
            received_tx.add(new_tx["id"])
            if id not in tangle:
                inboxes.insert_tx(new_tx)


class PingThread(Thread):
    def __init__(self, clientsocket):
        Thread.__init__(self)
        self.socket = clientsocket

    def run(self):
        cont = True
        self.socket.close()


class SetupThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.socket = None

    def run(self):
        global lamda
        global nu
        global neighbors
        global name
        global inbox_size
        global max_latency
        global simulation_time

        while self.socket == None:
            sleep(1)

        print("Setup phase begins")
        nodes_json = ""
        size = int.from_bytes(self.socket.recv(2), "big")
        print("To listen :")
        print(size)

        reception = self.socket.recv(size)
        nodes_json += reception.decode()

        print("Received all JSON")

        params = json.loads(nodes_json)
        lamda = params["lambda"]
        nu = params["nu"]
        inbox_size = params["inbox_size"]
        neighbors = params["neighbors"]
        name = params["name"]
        max_latency = params["max_latency"]
        simulation_time = params["simulation_time"]
        self.socket.send(b"Finished")
        self.socket.close()
        print("### End of Setup ###")

        while self.begin_master == False:
            sleep(0.5)

        print("### Let's begin the protocol ###\n")


class TangleThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global inboxes
        while continue_simulation == True:
            tx = inboxes.pop()
            if tx != None:
                tangle.add(tx["id"])
            sleep(0.5)


lamda = None
nu = None
inbox_size = None
neighbors = None
name = None
max_latency = None
simulation_time = None

continue_simulation = True
continue_analysis = True

tangle = set()
received_tx = set()
old_tx = []
recent_tx = []
inboxes = Inboxes()
inbox_sema = Semaphore()

tx_count = 0

setup = SetupThread()
setup.start()

server = ServerThread(setup)
server.start()
setup.join()

print("Name : " + name)
print("Inbox size : " + str(inbox_size))
print("Nu : " + str(nu))
print("Lambda : " + str(lamda))
print("Max latency : " + str(max_latency))
print("Simulation time : " + str(simulation_time))
print("Neighbors")
for n in neighbors:
    print("\t" + str(n["addr"]) + ":" + str(n["port"]))

starting_time = time.time()

generating = GeneratingThread()
generating.start()

tangling = TangleThread()
tangling.start()

analysis = AnalysisThread()
analysis.start()

generating.join()
tangling.join()

continue_analysis = False

analysis.join()
