import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind((socket.gethostname(), 0))

addr = serversocket.getsockname()[0]
port = serversocket.getsockname()[1]
print(str(addr) + ":" + str(port))

while True:
    serversocket.listen(2048)
    # accept connections from outside
    (clientsocket, address) = serversocket.accept()
    # now do something with the clientsocket
    # in this case, we'll pretend this is a threaded server

    a = clientsocket.recv(1)
    print(a)
    break
