import socket
import time
from sys import stdout
from time import sleep
import json
import networkx as nx
import matplotlib.pyplot as plt
from threading import Thread

cont = True
nodes = []
lamda = 3
nu = 3
inbox_size = 100
max_latency = 10
simulation_time = 60

open('ips.txt', 'w').close()


class BeginThread(Thread):
    def __init__(self, addr, port):
        Thread.__init__(self)
        self.addr = addr
        self.port = port

    def run(self):
        clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            clientsocket.connect((self.addr, self.port))
            clientsocket.send(b"B")
            clientsocket.close()
        except ConnectionRefusedError:
            print("Connection lost, ending the program")
            exit(1)


class SetupMasterThread(Thread):
    def __init__(self, addr, port, sending_data):
        Thread.__init__(self)
        self.addr = addr
        self.port = port
        self.sending_data = sending_data

    def run(self):
        jsonized = json.dumps(self.sending_data)
        clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            clientsocket.connect((self.addr, self.port))

            clientsocket.send(b"S")
            print(len(jsonized))
            size = len(jsonized)
            clientsocket.send(size.to_bytes(2, "big"))
            clientsocket.send(jsonized.encode("utf-8"))
            print("End sent")

            while 1:
                recv = clientsocket.recv(2048)
                if recv.decode() == "Finished":
                    break

            clientsocket.close()
        except ConnectionRefusedError:
            print("Connection lost, ending the program")
            exit(1)


class EndingThread(Thread):
    def __init__(self, name, addr, port):
        Thread.__init__(self)
        self.name = name
        self.addr = addr
        self.port = port

    def run(self):
        clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            clientsocket.connect((self.addr, self.port))

            clientsocket.send(b"E")

            return_content = ""

            while 1:
                tmp = clientsocket.recv(2048)
                if tmp == b"":
                    break
                else:
                    return_content += tmp.decode()
            clientsocket.close()
            print(return_content)
            f = open(self.name + ".txt", "w")
            f.write(return_content)
            f.close()
        except ConnectionRefusedError:
            print("Connection lost, ending the program")
            exit(1)


'''
while 1:
    source = input()
    if source == "end":
        break
    elif source == "":
        break
    else:
        get = source.split(":")
        new_addr = {"addr": get[0], "port": int(
            get[1]), "neighbors": [], "full_addr": source}

        nodes.append(new_addr)
'''
sleep(5)
print("On commence")

f = open("ips.txt")
for line in f:
    get = line.split(":")
    new_addr = {
        "addr": get[0],
        "port": int(get[1]),
        "neighbors": [],
    }
    nodes.append(new_addr)

names = ["chnafon", "gzor", "waldorg", "glargh",
         "schlipak", "loubet", "mliuej", "boulgourville", "valtordu", "kwzprtt", "ranuf", "noghall"]
N = len(nodes)
print(N)

k = 1

# Creating the network topology
G = nx.random_regular_graph(k, N)
nx.draw_networkx(G)
plt.savefig('graph.pdf')

# Seting nodes names and data
for (i, x) in enumerate(nodes):
    x["name"] = names[i]

for edge in G.edges:
    A = edge[0]
    B = edge[1]
    nodes[A]["neighbors"].append(
        {"addr": nodes[B]["addr"], "port": nodes[B]["port"]})
    nodes[B]["neighbors"].append(
        {"addr": nodes[A]["addr"], "port": nodes[A]["port"]})


# Seding the Setup signals
setup_threads = []
for (i, node) in enumerate(nodes):
    sending_data = {
        "neighbors": node["neighbors"],
        "name": node["name"],
        "lambda": lamda,
        "nu": nu,
        "inbox_size": inbox_size,
        "max_latency": max_latency,
        "simulation_time": simulation_time
    }

    setup_threads.append(SetupMasterThread(
        node["addr"], node["port"], sending_data))
    setup_threads[i].start()


print("### Waiting for all nodes to finish setup ###")

# Waiting for all
for thread in setup_threads:
    thread.join()


for node in nodes:
    new_thread = BeginThread(node["addr"], node["port"])
    new_thread.start()

for i in range(simulation_time):
    print("\r" + str(simulation_time + 2 - i), end="")
    sleep(1)

for node in nodes:
    new_thread = EndingThread(node["name"], node["addr"], node["port"])
    new_thread.run()
