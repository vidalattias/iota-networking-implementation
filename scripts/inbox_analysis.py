import matplotlib.pyplot as plt

N = 2

names = ["chnafon", "gzor", "waldorg", "glargh",
         "schlipak", "loubet", "mliuej", "boulgourville", "valtordu", "kwzprtt", "ranuf", "noghall"]


for i in range(N):
    inbox = []
    time = []
    f = open(names[i] + ".txt")
    for line in f:
        (t, inb) = line.split(";")
        time.append(int(t))
        inbox.append(int(inb))
    plt.plot(time, inbox)
plt.show()
